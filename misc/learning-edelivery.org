#+TITLE: ankify eDelivery
#+SOURCE: https://ec.europa.eu/cefdigital/wiki/display/CEFDIGITAL/eDelivery+AS4+-+1.14

* What is eDelivery AS4 Profile?
a modular profile of
ebMS3 and
AS4
OASIS specifications.

** What is AS4?
a profile of ebMS3 which sees itself as successor to AS2.

*** What is the intent of AS4?
ebMS3 has many alternatices to address a variety of scenarios.
AS4 shall bring continuity to the principles in ebMS3 and introduce
simplicity that made AS2 succesful.
** What is ebMS3?
a specification which defines a communications-protocol neutral method for exchanging electronic business messages.
*** What are the main components of ebMS3?
specific Web Services-based enveloping constructs 
a flexible enveloping technique, permitting messages to contain payloads of any format type.
** What is an OASIS specification?
a specification developed with OASIS OPEN community.

*** What is the OASIS OPEN community?
One of the most respected, non-profit standards bodies in the world, OASIS Open offers projects—including open source projects—a path to standardization and de jure approval for reference in international policy and procurement.
** What is the eDelivery AS4 Profile composed of?
a mandatory Common Profile and
a number of optional Profile Enhancement modules

*** What does the core/ common profile in eDelivery AS4 Profile do?
selects, extends and profiles the AS4 ebHandler Conformance Profile and AS4 Additional Features and provides a common Usage Profile

**** What is AS4 ebHandler Conformance Profile?
a specific conformance profile defined in the AS4 specification
***** What is an eBMS3 conformance profile?
A conformance profile in ebMS will define a class of implementations that may implement only a subset of ebMS3, and/or a particular set of options (e.g. transport protocol binding, SOAP version)
**** What is AS4 Additional Features?
a number of Additional Features defined in OASIS AS4 specification
**** What is an a Usage Profile in this context?
a usage profile defines how a standard should be used by a community of users
