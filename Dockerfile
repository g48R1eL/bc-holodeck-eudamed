FROM adoptopenjdk/openjdk16:jre-16.0.1_9-debian

RUN apt update && apt --assume-yes install ruby-full lighttpd

RUN mkdir /app
RUN chmod -R 755 /app
ADD target/app /app

RUN mkdir /conf
RUN chmod -R 755 /conf
ADD conf /conf

RUN mkdir /lighttpd
RUN chmod -R 755 /lighttpd
ADD additional_services/lighttpd /lighttpd
COPY additional_services/lighttpd/html/index.html /var/www/html/

COPY run.sh /run.sh
RUN chmod -R 755 /run.sh

WORKDIR /
EXPOSE 8080
EXPOSE 80
CMD ["sh", "run.sh"]