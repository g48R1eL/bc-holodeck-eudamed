REGISTRY = eu.gcr.io
PROJECT_ID ?= bc-kubernetes
IMAGE_NAME := $(REGISTRY)/$(PROJECT_ID)/bc-holodeck-app

GIT_SHA1 := $(shell git rev-parse --short=10 --verify HEAD)

UNAME := $(shell uname -m)
ifeq ($(UNAME), arm64)
  PLATFORM = --platform linux/amd64
else
  PLATFORM =
endif

clean:
	[ -d target ] && rm -r target

build: clean
	unzip holodeck-source/holodeckb2b-distribution-5.3.2.zip -d target
	mv target/holodeckb2b-5.3.2/ target/app
	cp holodeck-source/extensions/rest-empty-responses/rest-empty-error-1.1.0.mar target/app/lib
	cp holodeck-source/extensions/rest-backend/rest-backend-1.1.1.jar target/app/lib
	cp holodeck-source/extensions/rest-backend/hb2b-rest-backend.aar target/app/repository/services

docker: build
	docker build  $(PLATFORM) -t $(IMAGE_NAME):latest -t $(IMAGE_NAME):$(GIT_SHA1) .

push: docker
	docker push $(IMAGE_NAME):latest
	docker push $(IMAGE_NAME):$(GIT_SHA1)

deploy\:test: push
	kubectl --context gke_bc-kubernetes_europe-west1-b_test-1 set image deployment/eudamed-holodeck-app holodeck-app=$(IMAGE_NAME):$(GIT_SHA1)

deploy\:prod: push
	# echo "$(IMAGE_ON_TEST)"
	kubectl --context gke_bc-kubernetes_europe-west1-b_production-1 set image deployment/eudamed-holodeck-app holodeck-app=$(IMAGE_NAME):$(GIT_SHA1)

local: docker
	docker run -e EUDAMED_AS4_URL=local-eudamed-url -p 9090:8080 -a STDOUT eudamed-gateway-mock
