

# This p-mode is only needed to test the general connection to 
# the eudamed gateway acces point.
# Once it has been succesfully tested, there is no need for 
# the this p-mode.
erb /conf/pmodes/pmode-eudamed-push-init-ping-service.xml.erb \
> /app/repository/pmodes/pmode-eudamed-push-init-ping-service.xml

# This p-mode is only needed to test the general connection to 
# the eudamed gateway acces point.
# Once it has been succesfully tested, there is no need for 
# the this p-mode.
erb /conf/pmodes/pmode-eudamed-push-resp-eudamed-initiated-ping.xml.erb \
> /app/repository/pmodes/pmode-eudamed-push-resp-eudamed-ping.xml

# p-mode for initiating commmunication with a two way push
erb /conf/pmodes/pmode-eudamed-push-init.xml.erb \
> /app/repository/pmodes/pmode-eudamed-push-init.xml

# p-mode for processing a user message send via push
# from eudamed.
# We can trigger the push from eudamed by succesfully
# submitting a user message via two way push from us to
# eudamed.
erb /conf/pmodes/pmode-eudamed-push-resp-eudamed-initiated.xml.erb \
> /app/repository/pmodes/pmode-eudamed-push-resp-eudamed-initiated.xml

mv /conf/certs/$TRADING_PARTNER_CERTIFICATES_FILE_NAME \
/app/repository/certs/$TRADING_PARTNER_CERTIFICATES_FILE_NAME

mv /conf/certs/$TRUSTED_CERTIFICATES_FILE_NAME \
/app/repository/certs/$TRUSTED_CERTIFICATES_FILE_NAME

mv /conf/certs/$PRIVATE_KEYS_FILE_NAME \
   /app/repository/certs/$PRIVATE_KEYS_FILE_NAME

erb /conf/certs/certmanager_config.xml.erb \
> /app/conf/certmanager_config.xml
