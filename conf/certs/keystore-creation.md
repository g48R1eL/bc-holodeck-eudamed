1. Add our own certificate to private keys with the alias referenced in ConfigMap.
keytool -importkeystore -srcstoretype PKCS12 \
        -srckeystore bc-eudamed-as4-test.pfx \
        -srcalias bc-eudamed-as4-test \
        -srcstorepass ievahsh9pie9Bude \
        -destkeystore privatekeys_test_env.jks \
        -deststorepass secrets \
        -destalias bc-eudamed-as4-test \
        -destkeypass ievahsh9pie9Bude


2. Add eudamed public certificate to trading partner certs

keytool -importcert \
        -keystore partnercerts_test_env.jks \
        -storepass nosecrets \
        -alias eudamed_mdr_acc \
        -file EUDAMED_MDR_ACC_20210712.pem

3. Add trusted root certificate to trusted_certs keystore

keytool -importcert \
        -keystore trustedcerts_test_env.jks \
        -storepass trusted \
        -alias telesec_business_ca_1 \
        -file telesec_business_ca_1.crt


3. and 4. do the equivalent for production keystores.

5. in order to check the results:
keytool -list -v -storetype pkcs12 -keystore «path to certificate file»

6. This follows the instructions from holodeck
They can be found in :
[holodeck-root-dir]/repository/certs/README.md
