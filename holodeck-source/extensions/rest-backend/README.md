For our use case we had to extend the rest-backend a little bit.

The source for the extension can be found here:

https://github.com/fincarGit/rest-backend/tree/add_header_to_configure_payload_uri

This change is also part of the Pull request:

https://github.com/holodeck-b2b/rest-backend/pull/11

As bayard consulting we can not decide, when this pull request is merged.

Until then we will have to build rest-backend ourselves with the minor change.

Building this requires a little bit more effort, because we need to build other 
artifacts as well.

I put a small history of the necessary steps to build into this directory:
build-rest-backend-notes.org

However, after succesfully building this, I put the rest-backend-1.1.1.jar, hb2b-rest-backend.aar
also to this directory.
These are the ones we use when building our holodeck instance.

--> If the rest-backend needs to be updated, and the PR is not merged, we will have to 
make a similar patch for the future version.
